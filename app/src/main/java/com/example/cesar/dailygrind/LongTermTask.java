package com.example.cesar.dailygrind;

/**
 * Created by Useche on 4/10/16.
 */
public class LongTermTask {
    public String name;
    public String uid;
    public String start_date;
    public String due_date;
    public String start_time;
    public String due_time;
    public int exp_value;

    public LongTermTask() {
    }

    public LongTermTask(String name, String uid, String start_date, String start_time,String due_date, String due_time, int exp_value) {
        this.name = name;
        this.uid = uid;
        this.start_date = start_date;
        this.start_time = start_time;
        this.due_date = due_date;
        this.due_time = due_time;
        this.exp_value = exp_value;
    }

    public String getName() {
        return name;
    }

    public String getUid() { return uid; }

    public String getStart_date() {
        return start_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public String getDue_date() {
        return due_date;
    }

    public String getDue_time() {
        return due_time;
    }

    public boolean setName(String name) {
        this.name = name;
        return true;
    }

    public boolean setStart_date(String start_date) {
        this.start_date = start_date;
        return true;
    }

    public boolean setStart_time(String start_time) {
        this.start_time = start_time;
        return true;
    }

    public boolean setDue_date(String due_date) {
        this.due_date = due_date;
        return true;
    }

    public boolean setDue_time(String due_time) {
        this.due_time = due_time;
        return true;
    }

    public boolean setExp_value(int exp_value) {
        this.exp_value = exp_value;
        return true;
    }

    public int getExp_value() {
        return exp_value;
    }
}
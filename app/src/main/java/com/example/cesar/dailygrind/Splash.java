package com.example.cesar.dailygrind;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class Splash extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new CountDownTimer(2000, 1) {

            Intent intent = new Intent(Splash.this, UserSetup.class);
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                startActivity(intent);
            }
        }.start();
    }
}

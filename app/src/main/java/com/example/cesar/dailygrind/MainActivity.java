package com.example.cesar.dailygrind;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;


public class MainActivity extends Activity {
    public ShortTermFragment shortTermFragment = new ShortTermFragment();
    public LongTermFragment longTermFragment = new LongTermFragment();


    Firebase ref;
    Firebase userRef;
    TextView nameV;
    TextView levelV;
    Button logoutButton;
    int exp;

    Firebase getFirebaseRef() { return ref;}
    Firebase getFirebaseUserRef() { return userRef;}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ref = new Firebase(BuildConfig.FIREBASE_URL);
        ref.addAuthStateListener(new Firebase.AuthStateListener() {
            @Override
            public void onAuthStateChanged(AuthData authData) {
                if (authData == null) {
                    Intent intent = new Intent(MainActivity.this, UserSetup.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    User.instance = null;
                    finish();
                }
            }
        });

        // Store the user info in
        userRef = ref.child("users").child(ref.getAuth().getUid());
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User.instance = dataSnapshot.getValue(User.class);
                // User.instance.uid = dataSnapshot.getKey();

                // If user data is null (server error), log off
                if (User.instance == null) {
                    Log.d("MainActivity", "User data not found");
                    ref.unauth();
                    return;
                }
                int exp = User.instance.getExperience();
                int currentlevel = Integer.parseInt(User.instance.getCurrentLevel());
                if (exp > currentlevel * 2) {
                    exp = exp - currentlevel * 2;
                    userRef.child("experience").setValue(exp);
                    User.instance.setExperience(exp);
                    currentlevel = currentlevel + 1;
                    userRef.child("currentLevel").setValue(currentlevel);
                    User.instance.setCurrentLevel(currentlevel);
                }

                nameV = (TextView) findViewById(R.id.name);
                levelV = (TextView) findViewById(R.id.PlayerLevel);
                nameV.setText(User.instance.getUsername());
                levelV.setText(User.instance.getCurrentLevel());

                SharedPreferences sharedPreferences = getSharedPreferences("daily-grind", 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Username", User.instance.getUsername());
                editor.putString("Uid", ref.getAuth().getUid());
                editor.putString("Level", User.instance.getCurrentLevel());
                editor.putInt("Experience", User.instance.getExperience());
                editor.putInt("To Next Level", User.instance.getNextLevel());
                editor.apply();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });

        FragmentManager LongTaskManager = getFragmentManager();
        FragmentTransaction LongTransaction = LongTaskManager.beginTransaction();
        LongTransaction.add(R.id.longTermFragment,longTermFragment,"longTerm");

        FragmentManager ShortTaskManager = getFragmentManager();
        FragmentTransaction ShortTransaction = ShortTaskManager.beginTransaction();
        ShortTransaction.add(R.id.shortTermFragment, shortTermFragment, "shortTerm");

        ShortTransaction.commit();
        LongTransaction.commit();

        logoutButton = (Button) findViewById(R.id.logout);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onLogOff();
            }
        });

    }


    public void onLogOff() {

        ref.unauth();
    }

    public void doPositiveClick() {
        Toast.makeText(getApplicationContext(), "Congratulations!", Toast.LENGTH_SHORT).show();
        String name = LongTermFragment.name;
        final Firebase longTermRef= ref.child(ref.getAuth().getUid()).child("longterm_tasks").child(name);
        longTermRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                LongTermTask it = snapshot.getValue(LongTermTask.class);
                exp = it.getExp_value();
                int old_exp = User.instance.getExperience();
                old_exp = exp + old_exp;
                ref.child("users").child(ref.getAuth().getUid()).child("experience").setValue(old_exp);
            }

            @Override
            public void onCancelled(FirebaseError error) {
            }
        });
        longTermRef.removeValue();
        finish();
        overridePendingTransition(R.anim.fadeout, R.anim.fadein);
        startActivity(getIntent());
    }
    public void doPositiveClick_S() {
        Toast.makeText(getApplicationContext(), "Congratulations!", Toast.LENGTH_SHORT).show();
        String name = ShortTermFragment.name;
        Firebase longTermRef= ref.child(ref.getAuth().getUid()).child("shortterm_tasks").child(name);
        longTermRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                LongTermTask it = snapshot.getValue(LongTermTask.class);
                exp = it.getExp_value();
                int old_exp = User.instance.getExperience();
                old_exp = exp + old_exp;
                ref.child("users").child(ref.getAuth().getUid()).child("experience").setValue(old_exp);
            }

            @Override
            public void onCancelled(FirebaseError error) {
            }
        });
        longTermRef.removeValue();
        finish();
        overridePendingTransition(R.anim.fadeout,R.anim.fadein);
        startActivity(getIntent());
    }
    public void doNegativeClick() {
        Toast.makeText(getApplicationContext(), "Don't forget it!", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void LevelUp(View v){
        PopUp p = new PopUp();
        p.showPopup(this, v);
    }

}

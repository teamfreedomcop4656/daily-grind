package com.example.cesar.dailygrind;

import com.firebase.client.FirebaseError;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;

/**
 * Created by The Red Comet on 4/14/2016.
 */
public class UserSetup extends AppCompatActivity {
    private final static String TAG = "UserSetup";

    Firebase ref;
    EditText uEmail;

    EditText uPassword;

    ProgressDialog authProgressDialog;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_setup);

        ref = new Firebase(BuildConfig.FIREBASE_URL);
        ref.addAuthStateListener(new Firebase.AuthStateListener() {
            @Override
            public void onAuthStateChanged(AuthData authData) {
                if (authProgressDialog != null) {
                    authProgressDialog.dismiss();
                }
                if (authData != null) {
                    Intent intent = new Intent(UserSetup.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                    startActivity(intent);
                    finish();
                }
            }
        });

    }

    public void startLogin(View v) {

        boolean mError = false;

        uEmail = (EditText) findViewById(R.id.userEmail);
        uPassword = (EditText) findViewById(R.id.userPassword);

        String email = uEmail.getText().toString().trim().toLowerCase();
        String password = uPassword.getText().toString();


        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            uEmail.setError("Must be a valid email");
            mError = true;
        }
        else
        {
            uEmail.setError(null);
        }

        if (mError)
            return;

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        Firebase.AuthResultHandler authResultHandler = new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                if (authProgressDialog != null) {
                    authProgressDialog.dismiss();
                }
                Snackbar.make(coordinatorLayout, "Error: " + firebaseError.getMessage(), Snackbar.LENGTH_SHORT).show();


            }
        };

        ref.authWithPassword(email, password, authResultHandler);



        Log.i(TAG, "here the email: " + email +"heres the p: " + password );
        makeAuthProgressDialog();
    }


    private static final int registrationRequestCode = 1;
    public void startRegistration(View v) {
        Intent intent = new Intent(this, UserRegistration.class);
        startActivityForResult(intent, registrationRequestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "Got result from " + requestCode);
        if (requestCode == registrationRequestCode)
            if (resultCode == RESULT_OK) {
                // We can assume that the login will be correct
                Bundle bundle = data.getExtras();
                ref.authWithPassword(bundle.getString("email"), bundle.getString("password"), null);
                makeAuthProgressDialog();
            }
    }

    private void makeAuthProgressDialog() {
        authProgressDialog = new ProgressDialog(this);
        authProgressDialog.setMessage("Logging In");
        authProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        authProgressDialog.show();
    }

}






package com.example.cesar.dailygrind;

import android.app.AlarmManager;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.ui.FirebaseRecyclerAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by cesar on 4/8/2016.
 */

public class LongTermFragment extends Fragment {

    private Firebase ref = new Firebase(BuildConfig.FIREBASE_URL);
    private FirebaseRecyclerAdapter<LongTermTask, LongTaskViewHolder> mAdapter;
    private RecyclerView recycler;
    public static String name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.longterm_layout, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Firebase.setAndroidContext(getActivity());
        final List<LongTermTask> test_list = new ArrayList<LongTermTask>();
        Firebase longtermRef;
        for (int i = 1; i < CreateTaskActivity.long_counter; i++) {
            longtermRef = ref.child("longterm_tasks" + i);
            longtermRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    String task_name;
                    task_name = snapshot.child("task_name").getValue().toString();

                    String start_date;
                    start_date = snapshot.child("start_date").getValue().toString();

                    String due_date;
                    due_date = snapshot.child("due_date").getValue().toString();

                    String start_time;
                    start_time = snapshot.child("start_time").getValue().toString();

                    String due_time;
                    due_time = snapshot.child("due_time").getValue().toString();

                    SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss", Locale.US);
                    String start_datetime = start_date + start_time;
                    String due_datetime = due_date + due_time;
                    int exp_value = 0;
                    try {
                        Date start = format.parse(start_datetime);
                        Date due = format.parse(due_datetime);
                        long diff = start.getTime() - due.getTime();
                        long seconds = diff / 1000;
                        long minutes = seconds / 60;
                        long length = minutes / 60;
                        exp_value = (int) Math.log10(length)*4+5;
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    LongTermTask l_task = new LongTermTask(task_name, ref.getAuth().getUid(), start_date, due_date, start_time, due_time, exp_value);

                    Context context = getActivity().getApplicationContext();
                    PendingIntent mAlarmSender = PendingIntent.getBroadcast(context, 0, new Intent(context, MainActivity.class), 0);
                    Calendar c = Calendar.getInstance();
                    try {
                        c.setTime(format.parse(due_time));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    long firstTime = c.getTimeInMillis();
                    // Schedule the alarm!
                    AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
                    am.set(AlarmManager.RTC_WAKEUP, firstTime, mAlarmSender);

                    test_list.add(l_task);
                }

                @Override
                public void onCancelled(FirebaseError error) {
                }
            });
        }

        //REPLACE WITH ACTUAL DATA FROM FIREBASE
        super.onActivityCreated(savedInstanceState);

        Firebase longTermRef= ref.child(ref.getAuth().getUid()).child("longterm_tasks");

        recycler = (RecyclerView) getActivity().findViewById(R.id.l_list);

        recycler.setHasFixedSize(false);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(layoutManager);

        mAdapter = new FirebaseRecyclerAdapter<LongTermTask, LongTaskViewHolder>(LongTermTask.class, R.layout.lt_task, LongTaskViewHolder.class, longTermRef) {
            @Override
            public void populateViewHolder(LongTaskViewHolder longTaskViewHolder, LongTermTask task, int position) {
                boolean isUser = false;

                if(ref.getAuth().getUid().equals(task.getUid()))
                    isUser = true;

                longTaskViewHolder.setTaskData(task.getName(), task.getDue_date(), task.getDue_time(), isUser);
            }
        };

        recycler.setAdapter(mAdapter);
        recycler.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity().getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        mAdapter.getRef(position).getKey();
                        TextView task_name = (TextView) view.findViewById(R.id.task_name);
                        name = task_name.getText().toString();
                        MyDialogFragment f = new MyDialogFragment();
                        f.show(getFragmentManager(), "1");
                    }
                })
        );

    }
 }


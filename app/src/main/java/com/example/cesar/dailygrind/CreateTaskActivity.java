package com.example.cesar.dailygrind;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Useche on 4/10/16.
 *
 * to check firebase writting connection
 */

public class CreateTaskActivity extends AppCompatActivity {
    public static int long_counter = 1;
    public static int short_counter = 1;
    private final static String TAG = "CreateTaskActivity";
    private Firebase ref = new Firebase(BuildConfig.FIREBASE_URL);
    private Firebase ltask = ref.child(ref.getAuth().getUid()).child("longterm_tasks");

    Button addButton;
    Button backButton;
    EditText dateText;
    EditText endDate;
    DatePickerDialog dateDialog;
    DatePickerDialog endDialog;
    TimePicker startTime;
    TimePicker endTime;
    DateFormat dateFormat;
    DateFormat timeFormat;
    Spinner mySpinner;
    EditText TaskName;
    TextInputLayout endFloat;
    boolean same;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_task);
        Firebase.setAndroidContext(this);

        TaskName = (EditText) findViewById(R.id.task_name);
        dateText = (EditText) findViewById(R.id.task_date);
        dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        timeFormat = new SimpleDateFormat("HH:mm", Locale.US);
        endFloat = (TextInputLayout) findViewById(R.id.enddateFloat);
        endDate = (EditText) findViewById(R.id.end_date);
        startTime = (TimePicker) findViewById(R.id.startTime);
        endTime = (TimePicker) findViewById(R.id.endTime);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        final String formattedDate = df.format(c.getTime());

        mySpinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getBaseContext(), R.array.Task_Type, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_drop_down);
        mySpinner.setAdapter(adapter);

        Calendar calendar = Calendar.getInstance();
        dateDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dateText.setText(dateFormat.format(newDate.getTime()));
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        endDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                endDate.setText(dateFormat.format(newDate.getTime()));
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        dateText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dateDialog.show();
                return false;
            }
        });

        endDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                endDialog.show();
                return false;
            }
        });
        //short term is start & end time, and date
        //longterm start & end date, start & end time
        mySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mySpinner.getSelectedItem().toString().equals("Short Term")) {
                    dateText.setText(formattedDate);
                    endFloat.setVisibility(View.INVISIBLE);
                }
                else if (mySpinner.getSelectedItem().toString().equals("Long Term")) {
                    dateText.setText(formattedDate);
                    endFloat.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        addButton = (Button) findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = TaskName.getText().toString().trim();
                String start_date = dateText.getText().toString();
                String end_date = endDate.getText().toString();
                String type = mySpinner.getSelectedItem().toString();
                int startHour = startTime.getCurrentHour();
                int startMin = startTime.getCurrentMinute();
                int endHour = endTime.getCurrentHour();
                int endMin = endTime.getCurrentMinute();
                String SH = Integer.toString(startHour);
                String SM = Integer.toString(startMin);
                String EH = Integer.toString(endHour);
                String EM = Integer.toString(endMin);
                Calendar startD = Calendar.getInstance();
                Calendar endD = Calendar.getInstance();

                String month,day,year;
                String emonth,eday,eyear;
                int y=0,m=0,d = 0;
                int ey=0,em=0,ed = 0;
                if(type.equals("Short Term")){
                    month = start_date.substring(0, 2);
                    day = start_date.substring(3, 5);
                    year = start_date.substring(6, 10);
                    y = Integer.parseInt(year);
                    m = Integer.parseInt(month)-1;
                    d = Integer.parseInt(day);
                    startD.set(y, m, d, startHour, startMin);
                }
                if(type.equals("Long Term")){
                    month = start_date.substring(0, 2);
                    day = start_date.substring(3, 5);
                    year = start_date.substring(6, 10);
                    y = Integer.parseInt(year);
                    m = Integer.parseInt(month)-1;
                    d = Integer.parseInt(day);
                    startD.set(y, m, d, startHour, startMin);
                    try {
                        emonth = end_date.substring(0, 2);
                        eday = end_date.substring(3, 5);
                        eyear = end_date.substring(6, 10);
                        ey = Integer.parseInt(eyear);
                        em = Integer.parseInt(emonth) - 1;
                        ed = Integer.parseInt(eday);
                        endD.set(ey, em, ed, endHour, endMin);
                    }catch (Exception e){}
                }
                Date start = new Date();
                Date end = new Date();

                same = false;
                if (startHour < 10) {SH = "0".concat(SH);}
                if (startMin < 10) {SM = "0".concat(SM);}
                if (endHour < 10) {EH = "0".concat(EH);}
                if (endMin < 10) {EM = "0".concat(EM);}
                try {
                    start = timeFormat.parse(SH + ":" + SM);
                    end = timeFormat.parse(EH + ":" + EM);
                } catch (Exception e) {}

                String startTime = SH + ":" + SM;
                String endTime = EH + ":" + EM;
                boolean valid = true;

                Firebase taskRef;
                if (name.length() < 1) {
                    Toast.makeText(CreateTaskActivity.this, "You must enter a task name!", Toast.LENGTH_SHORT).show();
                    valid = false;
                } else if (type.equals("Type:")) {
                    Toast.makeText(CreateTaskActivity.this, "You must select a type for the Task!", Toast.LENGTH_LONG).show();
                    valid = false;
                }else if (type.equals("Short Term") && (end.before(start) || startTime.equals(endTime))) {
                    Toast.makeText(CreateTaskActivity.this, "End Time is set before or the same as Start Time.", Toast.LENGTH_SHORT).show();
                    valid = false;
                }else if (type.equals("Long Term") && end_date.length() < 1) {
                    Toast.makeText(CreateTaskActivity.this, "You must set an end date!", Toast.LENGTH_SHORT).show();
                    valid = false;
                } else if(type.equals("Long Term") && (endD.before(startD) || endD.equals(startD))){
                    Toast.makeText(CreateTaskActivity.this, "End date is set before Start date!", Toast.LENGTH_SHORT).show();
                    valid = false;
                }
                if(valid) {
                    if (type.equals("Long Term")) {
                        final String tName = name;
                        taskRef = ref.child(ref.getAuth().getUid()).child("longterm_tasks");
                        Log.d(TAG, "in long");
                        taskRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot taskSnapshot : dataSnapshot.getChildren()) {
                                    String l_name = taskSnapshot.getKey();
                                    Log.d(TAG, "this is the task name" + l_name);
                                    if (tName.equals(l_name)) {
                                       // Toast.makeText(CreateTaskActivity.this, "This task already exists",
                                        //        Toast.LENGTH_SHORT).show();
                                        same = true;
                                    }
                                }
                            }
                            @Override
                            public void onCancelled(FirebaseError firebaseError) {
                            }
                        });

                    } else if (type.equals("Short Term")) {
                        Log.d(TAG, "in short");
                        final String sName = name;
                        taskRef = ref.child(ref.getAuth().getUid()).child("shortterm_tasks");
                        taskRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Log.d(TAG, "in short 1");
                                int counter = 0;
                                for (DataSnapshot taskSnapshot : dataSnapshot.getChildren()) {
                                    ShortTermTask st = taskSnapshot.getValue(ShortTermTask.class);
                                    if (sName.equals(st.getName())) {counter++;}
                                    if(counter > 0){
                                     //   Toast.makeText(CreateTaskActivity.this, "This task already exists", Toast.LENGTH_SHORT).show();
                                        same = true;
                                        break;
                                    }
                                }
                            }
                            @Override
                            public void onCancelled(FirebaseError firebaseError) {
                            }
                        });
                    }
                }
                if(same && valid){
                    Toast.makeText(CreateTaskActivity.this, "This task already exists", Toast.LENGTH_SHORT).show();
                }
                else if (!same && valid) {
                    Log.d(TAG, "in 1");
                    if (type.equals("Long Term")) {
                        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyyHH:mm", Locale.US);
                        String start_datetime = start_date + startTime;
                        String due_datetime = end_date + endTime;
                        int exp_value = 0;
                        try {
                            Date start_ = format.parse(start_datetime);
                            Date due = format.parse(due_datetime);
                            long diff =  due.getTime()- start_.getTime();
                            long seconds = diff / 1000;
                            long minutes = seconds / 60;
                            long length = minutes / 60;
                            exp_value = (int) Math.log10(length)*4+5;
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        taskRef = ref.child(ref.getAuth().getUid()).child("longterm_tasks");
                        LongTermTask l_task = new LongTermTask(name, ref.getAuth().getUid(), start_date, startTime, end_date, endTime, exp_value);
                        taskRef.child(name).setValue(l_task, new Firebase.CompletionListener() {
                            @Override
                            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                                if (firebaseError != null) {
                                    Log.e(TAG, firebaseError.toString());
                                }
                            }
                        });
                    } else {
                        int exp_value = 0;
                        SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);
                        try {
                            Date start_ = format.parse(startTime);
                            Date due = format.parse(endTime);
                            long diff = due.getTime() - start_.getTime();
                            long seconds = diff / 1000;
                            long length = seconds / 60;
                            exp_value = (int) Math.log10(length) * 2 + 5;
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        taskRef = ref.child(ref.getAuth().getUid()).child("shortterm_tasks");
                        ShortTermTask s_task = new ShortTermTask(name, ref.getAuth().getUid(), startTime, endTime, exp_value);
                        taskRef.child(name).setValue(s_task, new Firebase.CompletionListener() {
                            @Override
                            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                                if (firebaseError != null) {
                                    Log.e(TAG, firebaseError.toString());
                                }
                            }
                        });
                    }
                    finish();
                }
                Log.d(TAG, "this is booleans status" + same);
            }
        });

        backButton = (Button) findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }});
        ref = new Firebase(BuildConfig.FIREBASE_URL);
        }
}

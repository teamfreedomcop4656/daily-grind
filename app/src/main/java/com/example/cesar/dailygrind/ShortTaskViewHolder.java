package com.example.cesar.dailygrind;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by The Red Comet on 4/18/2016.
 */
public class ShortTaskViewHolder  extends RecyclerView.ViewHolder {
    View mView;

    public ShortTaskViewHolder(View itemView) {
        super (itemView);
        mView = itemView;
    }

    public void setTaskData(String name, String date, String time, Boolean isUser){

        if (isUser) {
            TextView task_name_text = (TextView) mView.findViewById(R.id.task_name);
            task_name_text.setText(name);
            TextView due_date_text = (TextView) mView.findViewById(R.id.task_due_date);
            due_date_text.setText(date);
            TextView due_time_text = (TextView) mView.findViewById(R.id.task_due_time);
            due_time_text.setText(time);
        }

        else {
            RelativeLayout taskContainer = (RelativeLayout) mView.findViewById(R.id.task_container);

            RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(0, 0); // (width, height)
            taskContainer.setLayoutParams(params);

            taskContainer.setVisibility(mView.GONE);

        }


    }


}




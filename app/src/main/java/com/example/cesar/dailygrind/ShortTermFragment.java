package com.example.cesar.dailygrind;

import android.app.AlarmManager;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.ui.FirebaseRecyclerAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by cesar on 4/8/2016.
 */

public class ShortTermFragment extends Fragment {
    public static String name;
    private Firebase ref = new Firebase(BuildConfig.FIREBASE_URL);
    public Button addTask;
    private FirebaseRecyclerAdapter<ShortTermTask, ShortTaskViewHolder> mAdapterS;
    private RecyclerView recyclerS;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shortterm_layout, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        addTask = (Button) getActivity().findViewById(R.id.addTaskButton);
        addTask.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), CreateTaskActivity.class));
            }
        });

        Firebase.setAndroidContext(getActivity());
        final List<ShortTermTask> test_list = new ArrayList<ShortTermTask>();
        Firebase shorttermRef;
        for (int i = 1; i < CreateTaskActivity.short_counter; i++) {
            shorttermRef = ref.child("shortterm_tasks" + i);
            shorttermRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    String task_name;
                    task_name = snapshot.child("task_name").getValue().toString();

                    String start_time;
                    start_time = snapshot.child("start_time").getValue().toString();

                    String due_time;
                    due_time = snapshot.child("due_time").getValue().toString();

                    int exp_value = 0;
                    SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss", Locale.US);
                    try {
                        Date start = format.parse(start_time);
                        Date due = format.parse(due_time);
                        long diff = start.getTime() - due.getTime();
                        long seconds = diff / 1000;
                        long length = seconds / 60;
                        exp_value = (int) Math.log10(length) * 2 + 5;
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    ShortTermTask s_task = new ShortTermTask(task_name, ref.getAuth().getUid(), start_time, due_time, exp_value);

                    Context context = getActivity().getApplicationContext();
                    PendingIntent mAlarmSender = PendingIntent.getBroadcast(context, 0, new Intent(context, MainActivity.class), 0);
                    Calendar c = Calendar.getInstance();
                    try {
                        c.setTime(format.parse(due_time));
                    }
                    catch (ParseException e) {
                        e.printStackTrace();
                    }
                    long firstTime = c.getTimeInMillis();
                    // Schedule the alarm!
                    AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    am.set(AlarmManager.RTC_WAKEUP, firstTime, mAlarmSender);

                    test_list.add(s_task);
                }

                @Override
                public void onCancelled(FirebaseError error) {
                }
            });
        }

       super.onActivityCreated(savedInstanceState);

        Firebase shortTermRef= ref.child(ref.getAuth().getUid()).child("shortterm_tasks");

        recyclerS = (RecyclerView) getActivity().findViewById(R.id.s_list);

        recyclerS.setHasFixedSize(false);
        final LinearLayoutManager lManager = new LinearLayoutManager(getActivity());
        recyclerS.setLayoutManager(lManager);

        mAdapterS = new FirebaseRecyclerAdapter<ShortTermTask, ShortTaskViewHolder>(ShortTermTask.class, R.layout.st_task, ShortTaskViewHolder.class, shortTermRef) {
            @Override
            public void populateViewHolder(ShortTaskViewHolder shortTaskViewHolder, ShortTermTask task, int position) {
                boolean isUser = false;

                if(ref.getAuth().getUid().equals(task.getUid()))
                    isUser = true;

                shortTaskViewHolder.setTaskData(task.getName(), task.getStart_time(), task.getDue_time(), isUser);
            }
        };

        recyclerS.setAdapter(mAdapterS);
        recyclerS.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity().getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        TextView task_name = (TextView) view.findViewById(R.id.task_name);
                        name = task_name.getText().toString();
                        MyDialogFragment_S f = new MyDialogFragment_S();
                        f.show(getFragmentManager(), "1");
                    }
                })
        );
    }
    /*
    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PopUp p = new PopUp();
            p.showPopup(getActivity(),getView());
        }
    }*/
}


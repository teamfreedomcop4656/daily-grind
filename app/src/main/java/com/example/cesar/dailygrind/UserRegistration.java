package com.example.cesar.dailygrind;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.Map;

/**
 * Created by Useche on 4/20/16.
 */
public class UserRegistration extends AppCompatActivity {
    private final static String TAG = "UserRegistration";
    private Firebase ref = new Firebase(BuildConfig.FIREBASE_URL);

    ProgressDialog regProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_registration);
    }

    private boolean validate(){
        Boolean missing = false;
        Boolean emailInvalid = false;
        Boolean passwordInvalid = false;

        TextView usernameErr = (TextView) findViewById(R.id.nameError);
        TextView emailErr = (TextView) findViewById(R.id.emailError);
        TextView passErr = (TextView) findViewById(R.id.passError);
        TextView confirmErr = (TextView) findViewById(R.id.confirmError);
        TextView errorMsg = (TextView) findViewById(R.id.errorMessage);

        usernameErr.setVisibility(View.INVISIBLE);
        emailErr.setVisibility(View.INVISIBLE);
        passErr.setVisibility(View.INVISIBLE);
        confirmErr.setVisibility(View.INVISIBLE);
        errorMsg.setText("");


        // name error checking
        String username = ((EditText) findViewById(R.id.userName)).getText().toString().trim();
        if(username.equals("")){
            missing = true;
            usernameErr.setVisibility(View.VISIBLE);
        }
        else
            usernameErr.setVisibility(View.INVISIBLE);

        // email error checking
        String email = ((EditText) findViewById(R.id.userEmail)).getText().toString().trim().toLowerCase();
        if(!email.equals("")) {
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                emailErr.setVisibility(View.VISIBLE);
                emailInvalid = true;
            }
            else{
                emailErr.setVisibility(View.INVISIBLE);
            }
        }
        else{
            emailErr.setVisibility(View.VISIBLE);
            missing = true;
        }

        // password error checking
        String pass1 = ((EditText) findViewById(R.id.userPassword)).getText().toString();
        String pass2 = ((EditText) findViewById(R.id.userConfirmPassword)).getText().toString();

        if(pass1.equals("")){
            missing = true;
            passErr.setVisibility(View.VISIBLE);
        }

        if(pass2.equals("")){
            missing = true;
            confirmErr.setVisibility(View.VISIBLE);
        }

        if(!(pass1.equals(pass2))){
            passwordInvalid = true;
            confirmErr.setVisibility(View.VISIBLE);
        }


        if(missing){
            errorMsg.setText("Complete all fields");
            return false;
        }
        else if(emailInvalid){
            errorMsg.setText("Email is invalid");
            return false;
        }
        else if(passwordInvalid){
            errorMsg.setText("Invalid passwords");
            return false;
        }
        else
            return true;
    }

    public void onSubmit(View v) {
        makeRegProgressDialog();
        if (!validate()) {

            Log.i(TAG, "Weird does not validte");
            return;
        }

        final String username = ((EditText) findViewById(R.id.userName)).getText().toString();
        final String email = ((EditText) findViewById(R.id.userEmail)).getText().toString().trim().toLowerCase();
        final String password = ((EditText) findViewById(R.id.userPassword)).getText().toString();

        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        Log.i(TAG, "Weird one");
        ref.createUser(email, password, new Firebase.ValueResultHandler<Map<String, Object>>(){
            @Override
            public void onSuccess(Map<String, Object> result) {
                String uid = result.get("uid").toString();
                Log.i(TAG, "Weird two");

                // Sets the user data for the uid created for the user
                Firebase userInfoRef = ref.child("users").child(uid);

                //User: name, experience, currentLevel, nextLevel
                User user = new User(username, 0, 1, 2);
                userInfoRef.setValue(user);
                Log.d("UserRegistration", "User created with uid --> " + uid);

                Log.i(TAG, "Weird three");
                // Send result to login activity and automatically log in
                Intent data = new Intent();
                Bundle bundle = new Bundle();
                bundle.putString("email", email);
                bundle.putString("password", password);
                data.putExtras(bundle);

                if (getParent() == null)
                    setResult(RESULT_OK, data);
                else
                    UserRegistration.this.getParent().setResult(RESULT_OK, data);

                regProgressDialog.dismiss();
                finish();
            }

            @Override
            public void onError(FirebaseError firebaseError) {
                switch (firebaseError.getCode()) {
                    case FirebaseError.EMAIL_TAKEN:
                        Snackbar.make(coordinatorLayout, "Email already exits", Snackbar.LENGTH_SHORT).show();
                        break;
                }
                regProgressDialog.dismiss();
                Log.i(TAG, firebaseError.getMessage());
                Log.i(TAG, "There was an error");

            }
        });
    }


    private void makeRegProgressDialog() {
        regProgressDialog = new ProgressDialog(this);
        regProgressDialog.setMessage("Registering");
        regProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        regProgressDialog.show();
    }

}

package com.example.cesar.dailygrind;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

/**
 * Created by cesar on 4/13/2016.
 */
public class PopUp {
    public void showPopup(final Activity context,View v) {
        final int screenWidth = context.getWindow().getDecorView().getWidth();
        final int screenHeight = context.getWindow().getDecorView().getHeight();

        final int popupWidth = screenWidth /2;
        final int popupHeight = screenHeight /2;

        RelativeLayout viewGroup = (RelativeLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_layout, viewGroup);

        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new ColorDrawable());
        popup.showAtLocation(v, Gravity.CENTER, 0, 0);

        Button close = (Button) layout.findViewById(R.id.dismiss);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
        ImageButton expand = (ImageButton) layout.findViewById(R.id.expand);
        expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                if(popup.getHeight()==screenHeight) {
                    popup.setWidth(popupWidth);
                    popup.setHeight(popupHeight);
                    popup.showAtLocation(v, Gravity.CENTER, 0, 0);
                }
                else{
                    popup.setWidth(screenWidth);
                    popup.setHeight(screenHeight);
                    popup.showAtLocation(v, Gravity.CENTER, 0, 0);
                }
            }
        });
    }
}

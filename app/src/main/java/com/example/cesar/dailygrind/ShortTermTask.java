package com.example.cesar.dailygrind;

/**
 * Created by The Red Comet on 4/14/2016.
 */
public class ShortTermTask {
    public String name;
    public String uid;
    public String start_time;
    public String due_time;
    public int exp_value;

    public ShortTermTask() {
    }

    public ShortTermTask(String name, String uid, String start_time, String due_time, int exp_value) {
        this.name = name;
        this.uid = uid;
        this.start_time = start_time;
        this.due_time = due_time;
        this.exp_value = exp_value;
    }

    public String getName() {
        return name;
    }

    public String getUid() {
        return uid;
    }

    public String getDue_time() {
        return due_time;
    }

    public String getStart_time() {
        return start_time;
    }


    public boolean setName(String name) {
        this.name = name;
        return true;
    }

    public boolean setDue_time(String due_time) {
        this.due_time = due_time;
        return true;
    }

    public boolean setStart_time(String start_time) {
        this.start_time = start_time;
        return true;
    }
}
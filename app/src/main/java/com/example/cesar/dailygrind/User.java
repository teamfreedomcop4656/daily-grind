package com.example.cesar.dailygrind;


/**
 * Created by Useche on 4/20/16.
 */

public class User {
    public static User instance;

    private String username;
    private int experience;
    private int currentLevel;
    private int nextLevel;


    public User() {}

    public User(String username, int experience, int currentLevel, int nextLevel) {
        this.username = username;
        this.experience = experience;
        this.currentLevel = currentLevel;
        this.nextLevel = nextLevel;
    }


    public String getUsername() {
        return username;
    }

    public int getExperience() {
        return experience;
    }

    public String getCurrentLevel() {
        return Integer.toString(currentLevel);
    }

    public int getNextLevel() {
        return nextLevel;
    }



    public boolean setUserame(String username) {
        this.username = username;
        return true;
    }

    public boolean setExperience(int experience) {
        this.experience = experience;
        return true;
    }
    public boolean setCurrentLevel(int currentLevel){
        this.currentLevel = currentLevel;
        return true;
    }
    public boolean setNextLevel(int nextLevel){
        this.nextLevel = nextLevel;
        return true;
    }

}

